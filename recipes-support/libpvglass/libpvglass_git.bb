SUMMARY = "Userspace libraries for vglass backend."
SECTION = "libs"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=fc178bcd425090939a8b634d1d6a9594"

DEPENDS = " \
    xen-tools \
    virtual/libivc \
"

PACKAGE_ARCH = "${MACHINE_ARCH}"

PV = "0+git${SRCPV}"

SRC_URI = "git://gitlab.com/vglass/pv-display-helper.git;protocol=https;branch=master-next"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

do_compile() {
    oe_runmake libraries
}

do_install() {
    oe_runmake DESTDIR="${D}" libraries_install
}

PACKAGES =+ " \
    libpvdisplayhelper \
    libpvbackendhelper \
"

SOLIBS = ".so"
FILES_SOLIBSDEV = ""
FILES_libpvdisplayhelper += " \
    ${libdir}/libpvdisplayhelper.so \
"
FILES_libpvbackendhelper += " \
    ${libdir}/libpvbackendhelper.so \
"
