# Copyright (C) 2015 Assured Information Security, Inc.
# Recipe released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "Userspace library for accessing the InterVM Communication modules"
DESCRIPTION = " \
    This library provides a set of userspace functions for interacting with \
    the IVC (InterVM Communication) kernel modules. These functions can be used \
    to gain simple file and shared-memory communications channel between VMs \
    running on the Xen hypervisor. \
"
AUTHOR = "\
    Kyle J. Temkin <temkink@ainfosec.com> \
    Brendan Kerrigan <kerriganb@ainfosec.com> \
"
LIC_FILES_CHKSUM = "file://../../LICENSE;md5=1676875b46a1978a82e9a6816db4e8a5"
LICENSE = "BSD"

PV     = "1+git${SRCPV}"

SRC_URI = "git://gitlab.com/vglass/ivc.git;protocol=https;branch=master"
SRCREV = "${AUTOREV}"

PROVIDES = "virtual/libivc"

S = "${WORKDIR}/git/src/us"

inherit cmake

RDEPENDS_${PN} += "kernel-module-ivc"
RCONLFICTS_${PN} = "libivc2"
