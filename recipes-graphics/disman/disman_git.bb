DESCRIPTION = "Display Manager"

LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE;md5=f1e379c1d5588582790922b24502f0b8"

DEPENDS = " \
    dbus \
    qtbase \
    virtual/libivc \
    libpvglass \
    vglass \
"

PV = "0+git${SRCPV}"
SRC_URI = "git://gitlab.com/vglass/disman.git;protocol=https;branch=master"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

require recipes-qt/qt5/qt5.inc
inherit update-rc.d

INITSCRIPT_NAME = "disman"
INITSCRIPT_PARAMS = "defaults 99 0"

# Not using -tools pkg-split.
PACKAGES_remove += "${PN}-tools"
FILES_${PN} += " \
    ${OE_QMAKE_PATH_BINS} \
"
RDEPENDS_${PN} += " \
    libpvbackendhelper \
    bash \
"
