#!/bin/sh
#Auto modeset twice, as sometimes X11 discovers new resolutions
#(but doesn't use them) during the first --auto.
xrandr -display :0 --output virtual --auto
xrandr -display :0 --output virtual --auto
